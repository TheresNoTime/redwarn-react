/**
 * Additional ORES data from the ORES API.
 *
 * Schema was manually translated from test output. Since this schema is not RedWarn's,
 * keys are kept as is.
 *
 * We'll only ever need the data for `damaging` and `goodfaith`, but I'll include the
 * others for future use.
 * **/
export interface ORESData {

    /* Vandalism-related */
    damaging: {
        score: {
            prediction: boolean,
            probability: {
                "false": number,
                "true": number
            }
        }
    };
    draftquality?: {
        score: {
            prediction: string,
            probability: {
                OK: number,
                attack: number,
                spam: number,
                vandalism: number
            }
        }
    };
    goodfaith?: {
        score: {
            prediction: true,
            probability: {
                "false": number,
                "true": number
            }
        }
    };

    /* Others */
    articlequality?: {
        score: {
            prediction: string,
            probability: { [key: string] : number }
        }
    };
    articletopic?: {
        score: {
            prediction: string[],
            probability: { [key: string] : number }
        }
    };
    drafttopic?: {
        score: {
            prediction: string[],
            probability: { [key: string] : number }
        }
    };
    wp10?: {
        score: {
            prediction: string,
            probability: { [key: string] : number }
        }
    };
}

export default ORESData;