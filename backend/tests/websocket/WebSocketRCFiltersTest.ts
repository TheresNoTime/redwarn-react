import processWebSocketRCFilters from "../../src/websocket/WebSocketInfo";
import MWRecentChange from "../../../common/src/objects/MWRecentChange";

const exampleChange : MWRecentChange = {
    id: "0",
    bot: false,
    comment: "/* Career */ Added information",
    length: {new: 35825, old: 35209 }, // bytes = 616
    minor: false,
    namespace: 0,
    parsedcomment: "<a href=\"#Career\" style=\"color:gray;font-style:italic\">\u2192Career</a>: Added information",
    patrolled: false,
    revision: {new: 0, old: undefined},
    "server_name": "en.wikipedia.org",
    "server_script_path": "/w",
    "server_url": "https://en.wikipedia.org",
    timestamp: 1600953668,
    title: "Bob the Builder (character)",
    type: "edit",
    user: "RedWarn",
    wiki: "enwiki"
};

describe("WebSocketInfo Recent Changes Filters tests", () => {
    test("Testing values check", () => {
        expect(exampleChange.length.new - exampleChange.length.old).toEqual(616);
    });

    // Constant filters
    test("Constant filters (AlwaysRCFilter, NeverRCFilter) tests", () => {
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "always"}
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "never"}
        ])).toEqual(false);
    });

    // Wikipedia filters
    test("WikiRCFilter tests", () => {
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "wiki", wikis: ["enwiki"]}
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "wiki", wikis: ["jawiki"]}
        ])).toEqual(false);
    });
    test("DiffSizeRCFilter tests", () => {
        // Inclusive, same value
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 616}
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", max: 616}
        ])).toEqual(true);

        // Exclusive, same value
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 616, inclusive: false}
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", max: 616, inclusive: false}
        ])).toEqual(false);

        // Exclusive, passing values
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 615, inclusive: false}
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", max: 617, inclusive: false}
        ])).toEqual(true);

        // Inclusive, both `min` and `max` supplied
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 616, max: 616}
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 616, max: 616, inclusive: false}
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 615, max: 617, inclusive: false}
        ])).toEqual(true);

        // Both `min` and `max` supplied, failing maximum
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 614, max: 616, inclusive: false}
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 614, max: 615, inclusive: false}
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 614, max: 615}
        ])).toEqual(false);

        // Both `min` and `max` supplied, failing minimum
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 616, max: 617, inclusive: false}
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 617, max: 617, inclusive: false}
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {type: "diffsize", min: 617, max: 617}
        ])).toEqual(false);
    });

    // Conditional filters
    test("AndRCFilter tests", () => {
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "and",
                subfilters: [
                    { type: "wiki", wikis: [ "enwiki" ] },
                    { type: "diffsize", min: 600 }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "and",
                subfilters: [
                    { type: "wiki", wikis: [ "jawiki" ] },
                    { type: "diffsize", min: 600 }
                ]
            }
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "and",
                subfilters: [
                    { type: "wiki", wikis: [ "enwiki" ] },
                    { type: "diffsize", max: 600 }
                ]
            }
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "and",
                subfilters: [
                    { type: "wiki", wikis: [ "jawiki" ] },
                    { type: "diffsize", max: 600 }
                ]
            }
        ])).toEqual(false);
    });
    test("OrRCFilter tests", () => {
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "or",
                subfilters: [
                    { type: "wiki", wikis: [ "enwiki" ] },
                    { type: "diffsize", min: 600 }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "or",
                subfilters: [
                    { type: "wiki", wikis: [ "enwiki" ] },
                    { type: "diffsize", max: 600 }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "or",
                subfilters: [
                    { type: "wiki", wikis: [ "jawiki" ] },
                    { type: "diffsize", min: 600 }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "or",
                subfilters: [
                    { type: "wiki", wikis: [ "jawiki" ] },
                    { type: "diffsize", max: 600 }
                ]
            }
        ])).toEqual(false);
    });
    test("XorRCFilter tests", () => {
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "xor",
                subfilters: [
                    { type: "wiki", wikis: [ "enwiki" ] },
                    { type: "diffsize", min: 600 }
                ]
            }
        ])).toEqual(false);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "xor",
                subfilters: [
                    { type: "wiki", wikis: [ "enwiki" ] },
                    { type: "diffsize", max: 600 }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "xor",
                subfilters: [
                    { type: "wiki", wikis: [ "jawiki" ] },
                    { type: "diffsize", min: 600 }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "xor",
                subfilters: [
                    { type: "wiki", wikis: [ "jawiki" ] },
                    { type: "diffsize", max: 600 }
                ]
            }
        ])).toEqual(false);
    });

    // Nested conditionals

    test("Nested AndRCFilter inside OrRCFilter test", () => {
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "or",
                subfilters: [
                    {
                        type: "and",
                        subfilters: [
                            { type: "wiki", wikis: ["enwiki"] },
                            { type: "diffsize", min: 600 }
                        ]
                    },
                    {
                        type: "diffsize",
                        min: 200
                    }
                ]
            }
        ])).toEqual(true);
        expect(processWebSocketRCFilters(exampleChange, [
            {
                type: "or",
                subfilters: [
                    {
                        type: "and",
                        subfilters: [
                            { type: "wiki", wikis: ["enwiki"] },
                            { type: "diffsize", min: 600 }
                        ]
                    },
                    {
                        type: "diffsize",
                        min: 700
                    }
                ]
            }
        ])).toEqual(true);
    });
});