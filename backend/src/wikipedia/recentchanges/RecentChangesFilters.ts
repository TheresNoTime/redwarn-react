import MWRecentChange from "../../../../common/src/objects/MWRecentChange";

interface TypedRCFilter { type : string }

interface AlwaysRCFilter extends TypedRCFilter { type: "always" }
interface NeverRCFilter extends TypedRCFilter { type: "never" }
interface AndRCFilter extends TypedRCFilter { type: "and", subfilters: RCFilters[] }
interface OrRCFilter extends TypedRCFilter { type: "or", subfilters: RCFilters[] }
interface XorRCFilter extends TypedRCFilter { type: "xor", subfilters: RCFilters[] }
interface WikiRCFilter extends TypedRCFilter { type: "wiki", wikis: string[] }
interface DiffSizeRCFilter extends TypedRCFilter {
    type: "diffsize",
    min?: number,
    max?: number
    inclusive?: boolean
}

export type RCFilters =
    AlwaysRCFilter
    | NeverRCFilter
    | AndRCFilter
    | OrRCFilter
    | XorRCFilter
    | WikiRCFilter
    | DiffSizeRCFilter;

type RCFilterTypes = RCFilters["type"];
type RCFilterResolversMap = {
    [key in RCFilterTypes]: (change : MWRecentChange, args : RCFilters)
        => boolean;
};

export const RCFilterResolvers : RCFilterResolversMap = {
    always(): boolean {
        return true;
    },
    never(): boolean {
        return false;
    },
    and(change, args : AndRCFilter) : boolean {
        let final = true;
        for (const subfilter of args.subfilters) {
            final = final && processWebSocketRCFilters(change, [subfilter]);
        }
        return final;
    },
    or(change, args : OrRCFilter): boolean {
        let final = false;
        for (const subfilter of args.subfilters) {
            final = final || processWebSocketRCFilters(change, [subfilter]);
        }
        return final;
    },
    xor(change, args : XorRCFilter): boolean {
        let final : boolean;
        for (const subfilter of args.subfilters) {
            if (final == null)
                final = processWebSocketRCFilters(change, [subfilter]);
            else
                final = ((final ? 1 : 0) ^ (processWebSocketRCFilters(change, [subfilter]) ? 1 : 0)) === 1;
        }
        return final;
    },
    wiki(change, args : WikiRCFilter) : boolean {
        for (const wiki of args.wikis) {
            if (change.wiki === wiki)
                return true;
        }
        return false;
    },
    diffsize(change : MWRecentChange, args : DiffSizeRCFilter) : boolean {
        if (args.inclusive == null) args.inclusive = true;
        const bytes = Math.abs(change.length.new - change.length.old);
        if (args.min != null && args.max != null) {
            return (
                ((args.inclusive ?? true) ? bytes >= args.min : bytes > args.min)
                &&
                ((args.inclusive ?? true) ? bytes <= args.max : bytes < args.max)
            );
        } else if (args.min != null) {
            return ((args.inclusive ?? true) ? bytes >= args.min : bytes > args.min);
        } else if (args.max != null) {
            return ((args.inclusive ?? true) ? bytes <= args.max : bytes < args.max);
        } else {
            return true;
        }
    }
};

export default function processWebSocketRCFilters(change : MWRecentChange, filters : RCFilters[]) : boolean {
    let final = true;
    for (const filter of filters) {
        final = final && RCFilterResolvers[filter.type](change, filter);
    }
    return final;
}