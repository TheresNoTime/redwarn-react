import * as React from "react";
import {OptionsObject, ProviderContext, withSnackbar} from "notistack";
import RedWarnUI from "../RedWarnUI";

/**
 * The `SnackbarHandler` is a non-rendering component responsible for enqueuing
 * Snackbar messages from other classes.
 *
 * There should only be one SnackbarHandler, but whichever handler was last
 * rendered will be the active SnackbarHandler.
 **/
export class SnackbarHandler extends React.Component<ProviderContext, any, any> {

    /**
     * Shows a Snackbar to RedWarn's SnackbarProvider.
     *
     * @param text The text to show in the snackbar.
     * @param options Additional snackbar options.
     */
    showSnackbar(text : string, options : OptionsObject) : void {
        this.props.enqueueSnackbar(text, options);
    }

    /**
     * Renders the SnackbarHandler.
     **/
    render() : false {
        RedWarnUI.snackbarHandler = this;
        return false;
    }

}

/**
 * See {@link SnackbarHandler}.
 **/
export default withSnackbar(SnackbarHandler);