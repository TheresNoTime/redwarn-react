/** The RedWarn wordmark. **/
export const RedWarnWordmark = "images/RW_Wordmark.svg";
/** The RedWarn wordmark, but for dark backgrounds. **/
export const RedWarnWordmarkDark = "images/RW_Wordmark_Dark.svg";
/** The RedWarn wordmark, but completely white. **/
export const RedWarnWordmarkWhite = "images/RW_Wordmark_White.svg";