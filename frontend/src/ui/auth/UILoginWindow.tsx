import * as React from "react";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import {RWWordmarkAuto} from "../components/RWLogo";
import {IRWWindowProps} from "../../App";
import Button from "@material-ui/core/Button";

import "../style/login.css";

/**
 * The UILoginWindow is a RedWarn window with a login button that leads to an OAuth verification page,
 * which is used to request access from a Wikipedia user to use their account for RedWarn's tools.
 **/
export default class UILoginWindow extends React.Component<IRWWindowProps> {

    /**
     * Renders the window.
     **/
    render() : JSX.Element {
        return <Dialog
            className={"rw-login"}
            open={true}
            PaperProps={{style: {padding: "64px 0"}}}
            maxWidth={"xs"}>
            <DialogTitle style={{textAlign: "center"}}>
                <RWWordmarkAuto />
            </DialogTitle>
            <DialogContent className={"rw-login-content"}>
                <DialogContentText>Hello, and welcome to RedWarn!</DialogContentText>
                <DialogContentText>To continue, please log in with your Wikipedia account.</DialogContentText>
                <Button>
                    Log in with Wikipedia
                </Button>
            </DialogContent>
        </Dialog>;
    }

}