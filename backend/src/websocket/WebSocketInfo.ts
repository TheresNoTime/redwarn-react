import WebSocket from "ws";
import {RCFilters} from "../wikipedia/recentchanges/RecentChangesFilters";

interface WebSocketInfo {

    // Might add token info, etc. related to socket here
    authenticated: boolean;
    socket: WebSocket;

    rcFilters?: RCFilters[];

}

export default WebSocketInfo;