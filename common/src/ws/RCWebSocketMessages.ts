import MWRecentChange from "../objects/MWRecentChange";
import {ClientToServerMessage, ServerToClientMessage} from "./WebSocketMessage";
import { RCFilters } from "../../../backend/src/wikipedia/recentchanges/RecentChangesFilters";

export type RCWebSocketMessages =
    /* Server to Client messages */
    ServerRecentChange

    /* Client to Server messages */
    | ClientSetRCFilters;



export interface ClientSetRCFilters extends ClientToServerMessage {

    type: "setRCFilters";
    filters: RCFilters[];

}

export interface ServerRecentChange extends ServerToClientMessage {

    type: "recentChange";
    change: MWRecentChange;

}