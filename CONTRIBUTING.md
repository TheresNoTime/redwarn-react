# Contributing to RedWarn

We've made the general setup process easy. The only catch is that everything runs with Node, since it's a hassle to have to work with 

## Preparing the Environment
1. Ensure that you have [Node 12.8.2](https://nodejs.org/en/download/) (or later) and `npm` installed.
2. **(recommended)** Make sure that you have a GPG key set up alongside your Git client so you can sign your commits.

## Setting Up
RedWarn's build scripts are written in JavaScript for maximum cross-platform compatibility. This means all scripts are executed using Node. No need to worry about packages, since the build scripts rely only on Node modules. 

1. Clone the RedWarn repository.
2. To setup RedWarn for development, make sure that your `NODE_ENV` environment variable is unset or set to "development". Otherwise, set it to "production".
3. Download all required dependencies with the `setup` script.
   ```shell script
   # From "redwarn/"
   node scripts/setup.js
   ```
4. You can now begin developing using RedWarn.

In case you want to read the logs of the RedWarn Backend (which uses `bunyan` for logging), you'll also have to install `bunyan` globally and pipe the output there.
1. Install `bunyan` globally with the following:
   ```shell script
   npm install -g bunyan
   ```
2. Pipe the output of the running backend server to `bunyan`.
   ```shell script
   # This works on Windows too.
   npm run dev-start | bunyan
   ```

## Building RedWarn
1. If you're done making changes, the next step is to build RedWarn. There are three paths to take here.
   * If you're making changes to RedWarn for eventual production use, you need to use the `build` script at the root.
   * If you're making changes to only one end for eventual production use, you need to use the `build` script in whatever end you're using.
   * If you want to skip dependency checking, you can just directly run `npm run webpack` for the frontend, or `tsc` for the backend. **[end]**
2. Choose the appropriate build script. This is either the `build.js` script at the root folder's scripts, or the `build.js` at each end's scripts.
3. Execute that build script.
   ```shell script
   node scripts/build.js
   ```
4. If you built the backend, the output files should be in `backend/build/`. If you built the frontend, the output script should be in `frontend/build/redwarn.js`, and an updated script should be available in the `static/scripts` directory. Building the frontend will also automatically update the `index.html` of the frontend.
5. If you have the backend server running, you can now visit RedWarn using the port configured. By default, this is port `45990`.

## Developing with RedWarn
Since having to Dockerize everything is slow, you can just run RedWarn's backend server bare on your machine. To do that, follow the following steps:
1. Build the backend at least once with the instructions above on "Building RedWarn", or by running the following from the `backend/` directory.
   ```shell script
   npm run build
   ```
2. Run the RedWarn backend server with the following from the `backend/` directory.
   ```shell script
   npm start
   ```
   This should start the RedWarn backend server. Do note that since RedWarn uses [bunyan](https://www.npmjs.com/package/bunyan) for logging, the log output is in JSON. You can pass the output to `bunyan-cli` in case you want to read it.
4. The web server should now be available at your system's port `45990`. Opening it should bring you to the RedWarn frontend.
5. For every change in `frontend/` that requires rebuilding, you have to pack everything again. However, RedWarn's `index.html` changes depending on the commit, so you'll have to use the provided npm script instead of manually building with `webpack`.
   ```shell script
   npm run repack
   ```
6. Just repack again whenever you change `frontend` components.

## Additional Notes

* [Frontend] Every time you make a change, you'll have to rebuild the bundle or just run `webpack` again. When recompiling TypeScript or repacking the webpack bundle, you should just let the IDE do the work for you.
* Though you can theoretically run `index.html` as is, if you don't have the backend, it won't work. Watch out for that.
* The `static/` folder has a few static files, and while you might have the urge to get rid of them since we can instead just make a purely-React file, we do have to consider that it works as a polyfill if:
    * The user does not have a fast internet connection and cannot quickly load the RedWarn React script.
    * The user has JavaScript disabled.
    * The user's connection drops while loading the RedWarn React script.
* The scripts were made without Docker in mind, which means users who don't want to use Docker can just run everything as is (but that's not recommended, of course).
    
## Directory Tree
An explainer of files and directories.

- **Backend** (`backend`) - The RedWarn backend, which handles everything Wikipedia.
- **Frontend** (`frontend`) - The RedWarn frontend, which handles the RedWarn React script. It is made with React and a ton of TS JSX.
- **Commons** (`common`) - Common type definitions used by both frontend and backend. Both ends are configured to automatically compile with the commons. Because of this, the commons does not have a `tsconfig.json` file. However, it still has a `package.json` file due to ESLint. 
- **Static** (`static`) - RedWarn static files, which are usually served by the web handler normally.